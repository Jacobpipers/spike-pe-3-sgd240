// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spikePe3.h"
#include "spikePe3Character.h"
#include "spikePe3Projectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "Classes/PhysicsEngine/RadialForceComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);


//////////////////////////////////////////////////////////////////////////
// AspikePe3Character

AspikePe3Character::AspikePe3Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetHiddenInGame(true, true);
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.


	// Radial Force 
	FirstPersonRadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	FirstPersonRadialForceComponent->SetupAttachment(RootComponent);

	thruster = CreateDefaultSubobject<UPhysicsThrusterComponent>(TEXT("Thrust"));
	thruster->SetupAttachment(Mesh1P);


	IsForcePush = false;
	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AspikePe3Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		FP_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}


	Mesh1P->SetHiddenInGame(false, true);
	

}

void AspikePe3Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasSheildGrowingStarted)
	{
		if (isForceSheildIsGrowing)
		{
			if (IsThrustEnabled)
			{
				ForceShieldRadius+= (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
			}
			else
			{
				ForceShieldRadius+= ForceShieldGrowthRate * DeltaTime;
			}
			if (ForceShieldRadius >= MaxForceShieldRadius)
			{
				isForceSheildIsGrowing = false;
			}
		}
		else
		{
			if (IsThrustEnabled)
			{
				ForceShieldRadius -= (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
			}
			else
			{
				ForceShieldRadius-= ForceShieldGrowthRate * DeltaTime;
			}
			if (ForceShieldRadius <= 50) // TODO: ADD MIN RADIUS
			{
				isForceSheildIsGrowing = true;
			}
		}
		

		


	//	thruster->Activate(IsThrustEnabled);
		

		FirstPersonRadialForceComponent->Radius = ForceShieldRadius;
		UE_LOG(LogTemp, Warning, TEXT("Power: %f "), ForceShieldRadius);
	}
	
	auto movement = GetCharacterMovement();
	if (IsThrustEnabled)
	{
		movement->AddForce(FVector(0, 0, 100000));
		ClampVector(movement->Velocity, FVector(0,0,0), FVector(20000, 20000, 200000));
	}
	
	if (IsForcePush)
	{
		UWorld* const World = GetWorld();
		AspikePe3Character::ForcePush(World);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AspikePe3Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	
	PlayerInputComponent->BindAction("PowerThrust", IE_Pressed, this, &AspikePe3Character::EnableThrust);
	PlayerInputComponent->BindAction("PowerThrust", IE_Released, this, &AspikePe3Character::DisableThrust);
	
	/*PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);*/
	
	
	
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AspikePe3Character::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AspikePe3Character::OnFire);
		PlayerInputComponent->BindAction("Fire", IE_Released, this, &AspikePe3Character::OnFireReleased);

		PlayerInputComponent->BindAction("RightHandAttack", IE_Pressed, this, &AspikePe3Character::StartRadialFire);
		PlayerInputComponent->BindAction("RightHandAttack", IE_Released, this, &AspikePe3Character::FireRadialFire);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AspikePe3Character::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &AspikePe3Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AspikePe3Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AspikePe3Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AspikePe3Character::LookUpAtRate);
}


void AspikePe3Character::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		IsForcePush = true;
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AspikePe3Character::OnFireReleased()
{
	IsForcePush = false;
}

void AspikePe3Character::ForcePush(UWorld *const &World)
{

	TArray<FHitResult> FireHits;
	FVector StartLocation;
	FVector EndLocation;
	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_PhysicsBody);
	FCollisionShape CollisionShape;
	FCollisionQueryParams Params;

	CollisionShape = FCollisionShape::MakeBox(Box);

	StartLocation = FirstPersonCameraComponent->GetComponentLocation();
	EndLocation = StartLocation + (FirstPersonCameraComponent->GetForwardVector() * 300); // TODO Change to distance

	// Debug
	const FName TraceTag("MyTraceTag");

	World->DebugDrawTraceTag = TraceTag;

	Params.TraceTag = TraceTag;
	Params.bTraceComplex = false;
	Params.bTraceAsyncScene = true;
	Params.bReturnPhysicalMaterial = true;
	

	if (World)
	{
		bool bIsHit = World->SweepMultiByObjectType(FireHits, StartLocation, EndLocation, Rot, ObjectQueryParams, CollisionShape, Params);
//		bool bIsHit = World->LineTraceMultiByObjectType(FireHits, StartLocation, EndLocation, ObjectQueryParams, Params);

		if (bIsHit)
		{
//			UPhysicalMaterial* PhysicalMtl = FireHits.PhysMaterial.Get();
			const TArray<FHitResult> Impacts = FireHits;
			for (const FHitResult& Impact : Impacts)
			{
				//UE_LOG(LogTemp, Warning, TEXT("MyCharacter's FName is %s"), *Impact.GetActor()->GetFName.ToString());
				UPrimitiveComponent* PushComponent = Impact.GetComponent();
				if (PushComponent->IsSimulatingPhysics())
				{
					UE_LOG(LogTemp, Warning, TEXT("MyCharacter's FName is: %s "), *PushComponent->GetOwner()->GetName());
					FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
					PushComponent->AddForce(ForwardVector * ForcePower);
					
				}
			}
		}

		/*FVector hitLocation = FireHit.ImpactPoint;*/
	}
}


void AspikePe3Character::StartRadialFire()
{
	HasSheildGrowingStarted = true;
}

void AspikePe3Character::FireRadialFire()
{
	HasSheildGrowingStarted = false;
	ForceShieldRadius = 50; // TODO: Add Min Radius
	// Impulse
	if (FirstPersonRadialForceComponent != NULL)
	{
		FirstPersonRadialForceComponent->FireImpulse();
	}
}

void AspikePe3Character::EnableThrust()
{
	if (HasSheildGrowingStarted)
	{
		IsThrustEnabled = true;
		UE_LOG(LogTemp, Warning, TEXT("Thrust Enabled"));
	}
	else
	{
		IsThrustEnabled = true;
	
	}
	
	
}

void AspikePe3Character::DisableThrust()
{
	IsThrustEnabled = false;
	ACharacter::StopJumping();
}

void AspikePe3Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AspikePe3Character::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AspikePe3Character::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void AspikePe3Character::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AspikePe3Character::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AspikePe3Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AspikePe3Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AspikePe3Character::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AspikePe3Character::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AspikePe3Character::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AspikePe3Character::TouchUpdate);
	}
	return bResult;
}
