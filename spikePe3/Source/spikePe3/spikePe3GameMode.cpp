// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spikePe3.h"
#include "spikePe3GameMode.h"
#include "spikePe3HUD.h"
#include "spikePe3Character.h"

AspikePe3GameMode::AspikePe3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AspikePe3HUD::StaticClass();
}
