# Spike Report

## SPIKE PE 3 - Forces and Impulses

### Introduction

The core element of manipulating physics objects is to actually simulate physics!
Adding forces and impulses are the key ways we interact with physics objects.

### Goals

* The Spike Report should answer each of the Gap questions

In a new “demo” project (FPS C++ template):

* Place a number of simulating physics actors
* Starting with a basic FPS character, remove any notion of “shooting”
* Add the following abilities:
	*	On left click, the player emits a constant force in a forward arc
	*	On right click, the player emits a large impulse radially, hitting all nearby simulating components
		*	Holding right click down increases the range affected
		*	Letting go emits the “blast”
	*	Space bar, while held, should enable a Thrust on the player, instead of doing a “Jump” Thrus player into sky.


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Physics Bodies](https://docs.unrealengine.com/latest/INT/Engine/Physics/PhysicsBodies/Reference/index.html)
* [Add Force](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Components/UPrimitiveComponent/AddForce/index.html)
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Create FPS C++ Template

	* Use the First Person Sample Map. Includes Cubes that [Stimulating Physics](https://docs.unrealengine.com/latest/INT/Resources/ContentExamples/Physics/1_1/).

2. Add Force
	* Create A Function Called Force Push
		* Make A collision Shape:

```c++
				CollisionShape = FCollisionShape::MakeBox(Box);
```

		* Test what box overalaps using [Sweep Call](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Engine/UWorld/SweepMultiByObjectType/index.html).  

```c++
bool bIsHit = World->SweepMultiByObjectType(FireHits, StartLocation, EndLocation, Rot, ObjectQueryParams, CollisionShape, Params);
```

		* Check if Anything is hit than Loop through hit Objects and Apply Force as in code below.

```c++
if (bIsHit)
{
	const TArray<FHitResult> Impacts = FireHits;

	for (const FHitResult& Impact : Impacts)
	{
		UPrimitiveComponent* PushComponent = Impact.GetComponent();

		if (PushComponent->IsSimulatingPhysics())
		{
			FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
			PushComponent->AddForce(ForwardVector * ForcePower);
					
		}
	}
}
```

3. Add Radial Impulse
	* In The Character Constructer add A Radial Force Component 

		* Set the Impulse Strength on URadialForceComponent to 1000 (This can be changed to suit needs)
		* Set Force to 0. 
		* Make sure flag for Ignoring Owning actor is ticked in the URadialForceComponent Details.


```c++
// Radial Force 
FirstPersonRadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
FirstPersonRadialForceComponent->SetupAttachment(RootComponent);
```

* Map The Right Hand Attack as seen below

```c++
			PlayerInputComponent->BindAction("RightHandAttack", IE_Pressed, this, &AspikePe3Character::StartRadialFire);
			PlayerInputComponent->BindAction("RightHandAttack", IE_Released, this, &AspikePe3Character::FireRadialFire);
```

* Set Action Mapping as seen below 

	![](ReportImages/InputSettings.png)

	* On Pressed Set growth of Radial Impulse to true and in tick increase Radial Force Component Radius,

```c++
if (HasSheildGrowingStarted)
{
	if (isForceSheildIsGrowing)
	{
		if (IsThrustEnabled)
		{
			ForceShieldRadius+= (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
		}
		else
		{
			ForceShieldRadius+= ForceShieldGrowthRate * DeltaTime;
		}
		if (ForceShieldRadius >= MaxForceShieldRadius)
		{
			isForceSheildIsGrowing = false;
		}
	}
	else
	{
		if (IsThrustEnabled)
		{
			ForceShieldRadius -= (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
		}
		else
		{
			ForceShieldRadius-= ForceShieldGrowthRate * DeltaTime;
		}
		if (ForceShieldRadius <= 50) // TODO: ADD MIN RADIUS
		{
			isForceSheildIsGrowing = true;
		}
	}
	FirstPersonRadialForceComponent->Radius = ForceShieldRadius;
}
```

* On Release Fire Radial Impulse

4. Create a thrust

	* Map Modifier as seen below 

```c++
PlayerInputComponent->BindAction("PowerThrust", IE_Pressed, this, &AspikePe3Character::EnableThrust);
PlayerInputComponent->BindAction("PowerThrust", IE_Released, this, &AspikePe3Character::DisableThrust);
```

* if HasSheildGrowingStarted is false and IsThrustEnabled true in tick Add Impulse to character.


```c++
void AspikePe3Character::EnableThrust()  // This Could Do with A better name.
{
	IsThrustEnabled = true;
}

void AspikePe3Character::DisableThrust() // This Could Do with a better name.
{
	IsThrustEnabled = false;
}
```

* In Tick apply Impulse to Character. 

```c++
auto movement = GetCharacterMovement();
if (IsThrustEnabled)
{
	movement->AddForce(FVector(0, 0, 100));
	ClampVector(movement->Velocity, FVector(0,0,0), FVector(2000, 2000, 2000));
}
```


### What we found out

What is the difference between a Force and an Impulse?

A Force changes a object velocitey over time wheres a Impulse is a force applied at a specic time. Each having specific use casses.

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.